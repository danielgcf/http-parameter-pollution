package app;


public class ActionInput {

    private final Action value;

    private ActionInput(Action value) {
        this.value = value;
    }

    public Action getValue() {
        return value;
    }

    public static ActionInput of(String action) {

        if (action == null) {
            throw new IllegalArgumentException();
        }

        try {
           return new ActionInput(Action.valueOf(action.toUpperCase()));
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }

    public enum Action {
        TRANSFER, WITHDRAW
    }

}
