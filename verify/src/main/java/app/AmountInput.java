package app;

import java.math.BigInteger;

public class AmountInput {

    private static BigInteger INT_MIN_VALUE = BigInteger.ZERO;
    private static BigInteger INT_MAX_VALUE = BigInteger.valueOf(Integer.MAX_VALUE);

    private final BigInteger value;

    private AmountInput(BigInteger value) {
        this.value = value;
    }

    public BigInteger getValue() {
        return value;
    }

    public static AmountInput of(String amount) {

        if (amount == null) {
            throw new IllegalArgumentException();
        }

        BigInteger convertedAmout = new BigInteger(amount);
        if (convertedAmout.compareTo(INT_MIN_VALUE) < 0 || convertedAmout.compareTo(INT_MAX_VALUE) > 0) {
            throw new IllegalArgumentException();
        }
        return new AmountInput(convertedAmout);
    }
}
